#!/usr/bin/env python
# -*- coding: utf-8 -*-
# pep8: disable=E501,W504

"""This is a module providing the function "uv_geotiff2animation" that generates an animation from stacked wind fields in geotiff

This module relies on the package scikit-image to coarse-grain the fields to generate wind arrows.
If this package is not available, PIL is automatically used for an approximate solution.
Some OS-dependent manual parameters are set for Windows, Linux and Mac. They control the size / resolution of the animation.
The current values gave indentical results for those 3 OS, but can be adapted if needed.
For .mp4 output format, FFMPEG needs to be available. (It is already provided for those 3 OS in the folder 'extra')
"""

__version__ = "0.1.0"
__date__ = "2022/03/14"
__author__ = "Jérôme Dujardin"
__credits__ = ["Jérôme Dujardin"]
__maintainer__ = "Jérôme Dujardin"
__email__ = "jerome.dujardin@slf.ch"
__license__ = "AGPL"

# Python Standard Library
from platform import system as os_name

# Installed packages
import numpy as np
from matplotlib.animation import FFMpegWriter, PillowWriter
import matplotlib.pyplot as plt
from matplotlib import cm
try:
    # Function required for accurate calculation of coarser resolutions arrows
    from skimage.transform import downscale_local_mean as downscale
    skimage_available = True
except ImportError:
    # Approximation available in Python Standard Library
    from PIL import Image
    skimage_available = False
    print('For arrows in animation.py: Could not find package scikit-image. ' +
          'Will use PIL instead (but approximation).\n')
# Project modules
import modules.support_functions as func


# dpi and scaling factors of the OS
if os_name() == 'Darwin':
    dpi = 72
    os_scale = 2  # If this variable is not used, figures on MacOS are twice as big (px) as expected
    font_size_mult = 1.35  # For same font same, it is quite smaller on MacOS
elif os_name() == 'Windows':
    dpi = 96
    os_scale = 1
    font_size_mult = 1
elif os_name() == 'Linux':
    dpi = 96
    os_scale = 1
    font_size_mult = 1
else:
    dpi = 96
    os_scale = 1
    font_size_mult = 1


def uv_geotiff2animation(filepath_input_u_v_dem, filepath_output, file_format, ffmpeg_path):
    """Creates an animation from the geotiffs containing u, v, and dem."""

    plt.rcParams['animation.ffmpeg_path'] = ffmpeg_path

    # Read data
    u = func.read_geotiff(filepath_input_u_v_dem[0], [])
    u = u[0]
    v = func.read_geotiff(filepath_input_u_v_dem[1], [])
    v = v[0]
    vel = np.sqrt(u**2 + v**2)
    vmin = np.nanquantile(vel, 0.001)
    vmax = np.nanquantile(vel, 0.999)
    dem = func.read_geotiff(filepath_input_u_v_dem[2], [])
    dem = dem[0][0, :, :]
    # Set the writer
    fps = max(1, int(round(vel.shape[0] / 6)))  # animation will last 6 sec
    if file_format == 'gif':
        moviewriter = PillowWriter(fps=fps)
    elif file_format == 'mp4':
        codec = 'libx264'
        moviewriter = FFMpegWriter(fps=fps, codec=codec,
                                   extra_args=['-preset', 'slow', '-profile:v', 'high', '-level:v',
                                               '4.0', '-pix_fmt', 'yuv420p', '-crf', '12', '-codec:a', 'aac'])
    # Output movie will have a height of 1080 px, regardless of the shape of input data
    n, m = vel.shape[1:]
    ratio = m / n
    if ratio > 1920 / 1080:
        width = 1920
        height = round(width / ratio)
    else:
        height = 1080
        width = round(height * ratio)
    if height % 2 == 1:  # Make sure even number of rows and columns (for ffmpeg)
        height -= 1
    if width % 2 == 1:
        width -= 1
    height /= dpi
    width /= dpi
    # Wind direction will be shown with arrows. Number of arrows is constant regardless of input shape
    nsub_arrow = max(1, int(round(min(m, n) / 60)))
    x, y = np.meshgrid(np.arange(0, m, nsub_arrow), np.arange(0, n, nsub_arrow))
    # Initialize the figure
    fig = plt.figure(figsize=(width, height), dpi=dpi)
    ax = plt.Axes(fig, [0., 0., 1., 1.])
    ax.set_axis_off()
    fig.add_axes(ax)
    cmap = cm.get_cmap('viridis', 1024)
    im = ax.imshow(vel[0, :, :], interpolation='none', aspect='equal', vmin=vmin, vmax=vmax, cmap=cmap, zorder=0)
    levels = np.linspace(dem.min(), dem.max(), min(20, 2 + vel.shape[1]))
    n_level = levels.shape[0]
    contours = ax.contour(dem, levels, colors=['white'], linewidths=np.flip(np.linspace(0.1, 0.5, n_level)), zorder=1)
    # Add colorbar inside the plot
    #   Label does not work for colorbar in inset_axes, so manually text
    pos_x = (1 - 0.025 / ratio) * vel.shape[2] - 0.5
    pos_y = 0.84 * vel.shape[1] - 0.5
    txt = plt.text(pos_x, pos_y, 'wind speed (m/s)', rotation=90,
                   fontsize=18 * font_size_mult, fontname='Arial', verticalalignment='center', color='white')
    txt2 = plt.text(pos_x, pos_y, 'elevation (m.a.s.l)', rotation=90,
                    fontsize=18 * font_size_mult, fontname='Arial', verticalalignment='center', color='black')
    txt2.set_visible(False)
    axins = ax.inset_axes([1 - 0.06 / ratio, 0.01, 0.03 / ratio, 0.3])
    cb = fig.colorbar(im, cax=axins)  # option "aspect" does not work with inset_axes. So, inset_axes sets this aspect
    cb.ax.tick_params(labelsize=24 * font_size_mult, labelcolor='white')
    for tick in cb.ax.get_xticklabels():
        tick.set_fontname("Arial")
    axins.yaxis.tick_left()

    # Loop on data frames
    moviewriter.setup(fig, str(filepath_output), dpi=fig.get_dpi() / os_scale)
    for j in range(3 * vel.shape[0]):
        i = j % vel.shape[0]
        if j < 2 * vel.shape[0]:
            im.remove()
            im = ax.imshow(vel[i, :, :], zorder=0)
        elif j == 2 * vel.shape[0]:
            txt.set_visible(False)
            txt2.set_visible(True)
            im.remove()
            for con in contours.collections:
                con.remove()
            im = ax.imshow(dem, cmap='Greys_r', zorder=0)
            cb2 = fig.colorbar(im, cax=axins)
            cb2.ax.tick_params(labelsize=24 * font_size_mult, labelcolor='black')
            axins.yaxis.tick_left()
        if j >= vel.shape[0]:
            # Coarse-grain u,v data to plot arrows
            if skimage_available:
                u_arrow = downscale(u[i, :, :], (nsub_arrow, nsub_arrow))
                v_arrow = downscale(v[i, :, :], (nsub_arrow, nsub_arrow))
            else:
                u_pil, v_pil = u[i, :, :], v[i, :, :]
                uv_min, uv_max = min(np.nanmin(u_pil), np.nanmin(v_pil)), max(np.nanmax(u_pil), np.nanmax(v_pil))
                u_pil = ((u_pil - uv_min) / (uv_max - uv_min) * 254 + 1).astype(np.uint8)  # +1 to avoid 0s from rounding
                v_pil = ((v_pil - uv_min) / (uv_max - uv_min) * 254 + 1).astype(np.uint8)
                u_pil, v_pil = Image.fromarray(u_pil), Image.fromarray(v_pil)
                new_size = (round(u_pil.size[0] / nsub_arrow), round(u_pil.size[1] / nsub_arrow))
                u_pil, v_pil = u_pil.resize(new_size), v_pil.resize(new_size)
                u_arrow, v_arrow = np.array(u_pil, dtype=np.float32), np.array(v_pil, dtype=np.float32)
                u_arrow = (u_arrow - 1) / 254 * (uv_max - uv_min) + uv_min
                v_arrow = (v_arrow - 1) / 254 * (uv_max - uv_min) + uv_min
            # Scale the arrows to have a constant length
            vel_arrow = np.sqrt(u_arrow**2 + v_arrow**2)
            u_arrow /= vel_arrow
            v_arrow /= vel_arrow
            n_arrow = vel_arrow.shape[0]
            #   0 values for vel (impossible with Wind-Topo) trigger a warning, and only those arrows are not plotted: good
            scale_arrow = 1.5 * n_arrow
            q = ax.quiver(x, y, u_arrow, v_arrow, units='height', scale=scale_arrow, width=0.075 / scale_arrow,
                          headwidth=5, headlength=5, headaxislength=5, color=[0.8, 0, 0])
        moviewriter.grab_frame()
        if j >= vel.shape[0]:
            q.remove()
    moviewriter.finish()
    plt.close()

#!/usr/bin/env python
# -*- coding: utf-8 -*-
# pep8: disable=E501,W504

"""This is a module providing functions essential to prepare the inputs for the Wind-Topo model.

Those functions were used during training of the model, thus ensuring that the correct inputs
are generated when Wind-Topo is used for downscaling.
"""

__version__ = "0.1.0"
__date__ = "2022/03/14"
__author__ = "Jérôme Dujardin"
__credits__ = ["Jérôme Dujardin"]
__maintainer__ = "Jérôme Dujardin"
__email__ = "jerome.dujardin@slf.ch"
__license__ = "AGPL"

# Python Standard Library
import os
from datetime import datetime

# Installed packages
from osgeo import gdal, gdal_array
import numpy as np
import torch

# Project modules
from progsettings import t_device, t_type


def compute_slope_aspect(dem, res):
    """Computes the slope and trigonometric aspect of the input 2D array 'dem' with spatial resolution 'res'."""

    fileout_temp = 'slope_aspect_temp'
    aspect = gdal.DEMProcessing(str(fileout_temp),
                                gdal_array.OpenNumPyArray(dem / res, False),
                                'aspect', zeroForFlat=True,
                                trigonometric=True).ReadAsArray()
    aspect *= np.pi / 180
    aspect[0, :] = aspect[1, :]  # gdal generates wrong values
    aspect[-1, :] = aspect[-2, :]
    aspect[:, 0] = aspect[:, 1]
    aspect[:, -1] = aspect[:, -2]

    slope = gdal.DEMProcessing(str(fileout_temp),
                               gdal_array.OpenNumPyArray(dem / res, False),
                               'slope', zeroForFlat=True,
                               trigonometric=True).ReadAsArray()
    slope *= np.pi / 180
    slope[0, :] = slope[1, :]  # gdal generates wrong values
    slope[-1, :] = slope[-2, :]
    slope[:, 0] = slope[:, 1]
    slope[:, -1] = slope[:, -2]

    os.remove(fileout_temp)

    return (slope, aspect)


def generate_time_features(timestamp):
    """Generates for each datetime in the list 'timestamp' 4 time features.

    Those features are the sine and cosine of the time of the year and sine and cosine of the time of the day.
    Values are thus between -1 and 1.
    For time of year, sin=0 and cos=1 for June 21st (summer solstice)
    For time of day, sin=0 and cos=1 for 13h30 (solar zenith)

    Returns 'time_feature', a numpy array (n_times, 4)
    """

    hour_in_year = [(t - datetime(timestamp[0].year - 1, 6, 21, 0)).seconds / 3600 +
                    (t - datetime(timestamp[0].year - 1, 6, 21, 0)).days * 24
                    for t in timestamp]
    hour_in_year = np.asarray(hour_in_year, dtype=np.float32)
    sin_year = np.sin((hour_in_year) / (365.25 * 24) * 2 * np.pi)
    cos_year = np.cos((hour_in_year) / (365.25 * 24) * 2 * np.pi)

    hour_in_day = [t.hour for t in timestamp]
    hour_in_day = np.asarray(hour_in_day, dtype=np.float32)
    sin_day = np.sin((hour_in_day - 13.5) / 24 * 2 * np.pi)  # 13h30 is the average solar zenith time
    cos_day = np.cos((hour_in_day - 13.5) / 24 * 2 * np.pi)

    time_feature = np.vstack((sin_year, cos_year, sin_day, cos_day)).T

    return time_feature


def fovel_blur_info(topo_patch_size, use_pytorch):
    """Generates the (static) info needed to apply a foveal blur on Wind-Topo topographic input patches.

    'topo_patch_size' : nb of rows and columns of the patches on which the blur will be applied.
    'use_pytorch': boolean indicating if the blur will be performed using pytorch functions (and so, possibly on GPU)

    Returns 'info', a tupple with all needed variables.
    """

    n, m = tuple(topo_patch_size)
    xx, yy = np.meshgrid(np.arange(m) - (m - 1) / 2, np.arange(n) - (n - 1) / 2)
    d = np.sqrt((xx**2 + yy**2)) / ((max(n, m) - 1) / 2)  # Distance from center [0, 1]
    s = (0.4 + 1.25 * d)**2  # Std of gaussian kernel
    p, q = 19, 19
    n_iter = 2

    p_half = int((p - 1) / 2)
    q_half = int((q - 1) / 2)
    x_kernel, y_kernel = np.meshgrid(np.arange(p) - p_half, np.arange(q) - q_half)
    kernel = np.exp(- (x_kernel[None, None, :, :]**2 + y_kernel[None, None, :, :]**2) /
                    (2 * s[p_half:-p_half, q_half:-q_half, None, None]**2))  # 2D gaussian
    kernel = kernel / np.sum(kernel, axis=(2, 3), keepdims=True)

    # Borders (bands)
    A = np.repeat(np.arange(0, p_half)[None, :, None] / (p_half - 1), m, axis=2)
    B = np.repeat(np.arange(p_half - 1, -1, -1)[None, :, None] / (p_half - 1), m, axis=2)
    C = np.repeat(np.arange(q_half - 1, -1, -1)[None, None, :] / (q_half - 1), n, axis=1)
    D = np.repeat(np.arange(0, q_half)[None, None, :] / (q_half - 1), n, axis=1)

    # Upper left corner
    w1_ul = np.repeat(np.arange(p_half)[:, None] / (p_half - 1), q_half, axis=1)
    w2_ul = np.repeat(np.arange(q_half)[None, :] / (q_half - 1), p_half, axis=0)
    w12_ul = w1_ul + w2_ul
    w12_ul[0, 0] = 1
    w1_ul /= w12_ul
    w2_ul /= w12_ul
    w1_ul = w1_ul[None, :, :]
    w2_ul = w2_ul[None, :, :]
    # Lower left corner
    w1_ll = np.repeat(np.arange(p_half - 1, -1, -1)[:, None] / (p_half - 1), q_half, axis=1)
    w2_ll = np.repeat(np.arange(q_half)[None, :] / (q_half - 1), p_half, axis=0)
    w12_ll = w1_ll + w2_ll
    w12_ll[-1, 0] = 1
    w1_ll /= w12_ll
    w2_ll /= w12_ll
    w1_ll = w1_ll[None, :, :]
    w2_ll = w2_ll[None, :, :]
    # Upper right corner
    w1_ur = np.repeat(np.arange(p_half)[:, None] / (p_half - 1), q_half, axis=1)
    w2_ur = np.repeat(np.arange(q_half - 1, -1, -1)[None, :] / (q_half - 1), p_half, axis=0)
    w12_ur = w1_ur + w2_ur
    w12_ur[0, -1] = 1
    w1_ur /= w12_ur
    w2_ur /= w12_ur
    w1_ur = w1_ur[None, :, :]
    w2_ur = w2_ur[None, :, :]
    # Lower right corner
    w1_lr = np.repeat(np.arange(p_half - 1, -1, -1)[:, None] / (p_half - 1), q_half, axis=1)
    w2_lr = np.repeat(np.arange(q_half - 1, -1, -1)[None, :] / (q_half - 1), p_half, axis=0)
    w12_lr = w1_lr + w2_lr
    w12_lr[-1, -1] = 1
    w1_lr /= w12_lr
    w2_lr /= w12_lr
    w1_lr = w1_lr[None, :, :]
    w2_lr = w2_lr[None, :, :]

    if use_pytorch:
        with torch.no_grad():
            # Somehow, exec() is not taken into account, so had to do it one by one
            # for var in ['kernel', 'A', 'B', 'C', 'D', 'w1_ul', 'w2_ul', 'w1_ll',
            #             'w2_ll', 'w1_ur', 'w2_ur', 'w1_lr', 'w2_lr']:
            #     exec(var + '=' + 'torch.tensor(' + var + ', dtype=t_type, device= t_device)')
            kernel = torch.tensor(kernel, dtype=t_type, device=t_device)
            A = torch.tensor(A, dtype=t_type, device=t_device)
            B = torch.tensor(B, dtype=t_type, device=t_device)
            C = torch.tensor(C, dtype=t_type, device=t_device)
            D = torch.tensor(D, dtype=t_type, device=t_device)
            w1_ul = torch.tensor(w1_ul, dtype=t_type, device=t_device)
            w2_ul = torch.tensor(w2_ul, dtype=t_type, device=t_device)
            w1_ll = torch.tensor(w1_ll, dtype=t_type, device=t_device)
            w2_ll = torch.tensor(w2_ll, dtype=t_type, device=t_device)
            w1_ur = torch.tensor(w1_ur, dtype=t_type, device=t_device)
            w2_ur = torch.tensor(w2_ur, dtype=t_type, device=t_device)
            w1_lr = torch.tensor(w1_lr, dtype=t_type, device=t_device)
            w2_lr = torch.tensor(w2_lr, dtype=t_type, device=t_device)

    info = (p, q, p_half, q_half, n_iter, kernel, A, B, C, D,
            w1_ul, w2_ul, w1_ll, w2_ll, w1_ur, w2_ur, w1_lr, w2_lr, use_pytorch)

    return info


def foveal_blur(im, info):
    """Apply the foveal blur defined in the function fovel_blur_info to each channel of the array 'im' (channels, n, m)"""

    (p, q, p_half, q_half, n_iter, kernel, A, B, C, D,
     w1_ul, w2_ul, w1_ll, w2_ll, w1_ur, w2_ur, w1_lr, w2_lr, use_pytorch) = info

    if use_pytorch:
        my_moveaxis = torch.moveaxis
        my_sum = torch.sum
        my_tile = torch.tile
        my_mean = torch.mean
    else:
        my_moveaxis = np.moveaxis
        my_sum = np.sum
        my_tile = np.tile
        my_mean = np.mean

    with torch.no_grad():

        if use_pytorch:
            im_output = torch.tensor(im, dtype=t_type, device=t_device)
            im_init = im_output.clone()
            c, n, m = im_output.shape
            im2 = torch.empty((p, q, c, n - 2 * p_half, m - 2 * q_half), dtype=t_type, device=t_device)
        else:
            im_output = im.copy()
            im_init = im_output.copy()
            c, n, m = im_output.shape
            im2 = np.empty((p, q, c, n - 2 * p_half, m - 2 * q_half), dtype=np.float32)

        for _ in range(n_iter):
            for i in range(p):
                for j in range(q):
                    im2[i, j, :, :, :] = im_output[:, i:n - (2 * p_half - i), j:m - (2 * q_half - j)]
            im3 = my_moveaxis(im2, (0, 1), (-2, -1))
            im3 = my_sum(im3 * kernel[None, :, :, :, :], (3, 4))
            im_output[:, p_half:-p_half, q_half:-q_half] = im3
            del im3

            # Upper band
            aa = my_tile(my_mean(im_init[:, :p_half, :], (1, 2), keepdims=True), (1, p_half, m)) * B + \
                im_output[:, [p_half], :] * A
            # Lower band
            bb = my_tile(my_mean(im_init[:, -p_half:, :], (1, 2), keepdims=True), (1, p_half, m)) * A + \
                im_output[:, [- p_half - 1], :] * B
            # Left band
            cc = my_tile(my_mean(im_init[:, :, :q_half], (1, 2), keepdims=True), (1, n, q_half)) * C + \
                im_output[:, :, [q_half]] * D
            # Right band
            dd = my_tile(my_mean(im_init[:, :, -q_half:], (1, 2), keepdims=True), (1, n, q_half)) * D + \
                im_output[:, :, [- q_half - 1]] * C

            # Use only the central part of the band (corners removed)
            im_output[:, :p_half, q_half:-q_half] = aa[:, :, q_half:-q_half]
            im_output[:, -p_half:, q_half:-q_half] = bb[:, :, q_half:-q_half]
            im_output[:, p_half:-p_half, :q_half] = cc[:, p_half:-p_half, :]
            im_output[:, p_half:-p_half, -q_half:] = dd[:, p_half:-p_half, :]

            # Upper left corner
            val = w1_ul * im_output[:, [p_half], :q_half] + w2_ul * im_output[:, :p_half, [q_half]]
            val[:, 0, 0] = (val[:, 0, 1] + val[:, 1, 0] + val[:, 1, 1]) / 3
            im_output[:, :p_half, :q_half] = val
            # Lower left corner
            val = w1_ll * im_output[:, [-p_half - 1], :q_half] + w2_ll * im_output[:, -p_half:, [q_half]]
            val[:, -1, 0] = (val[:, -1, 1] + val[:, -2, 0] + val[:, -2, 1]) / 3
            im_output[:, -p_half:, :q_half] = val
            # Upper right corner
            val = w1_ur * im_output[:, [p_half], -q_half:] + w2_ur * im_output[:, :p_half, [-q_half - 1]]
            val[:, 0, -1] = (val[:, 0, -2] + val[:, 1, -2] + val[:, 1, -1]) / 3
            im_output[:, :p_half, -q_half:] = val
            # Lower right corner
            val = w1_lr * im_output[:, [-p_half - 1], -q_half:] + w2_lr * im_output[:, -p_half:, [-q_half - 1]]
            val[:, -1, -1] = (val[:, -2, -1] + val[:, -1, -2] + val[:, -2, -2]) / 3
            im_output[:, -p_half:, -q_half:] = val
        del im2, im_init
        if use_pytorch:
            im_output = im_output.cpu().numpy()

    return im_output


def standardize(x, x_mean, x_std_inv, type_list, deep_copy):
    """Standardizes the variables in dict 'x' specified by 'type_list'.
    'x_mean' and 'x_std' are dict with the same keys as 'x', containing the required variables in the correct
    format: (1, n_layer, n_row, n_col)
    'deep_copy' is a boolean that triggers a deep copy of x. If false, x will be modified.

    Returns 'x_out', either a new dict or just an alias of x depending on 'deep_copy'.
    """

    if deep_copy:
        x_out = dict()
        for k in x.keys():
            x_out[k] = x[k].copy()
    else:
        x_out = x

    for var_type in type_list:
        # x_out[var_type] = (x[var_type] - x_mean[var_type]) * x_std_inv[var_type]
        # Below: faster version, if enough means and std at 0 and 1 respectively
        n_var = x_out[var_type].shape[1]
        ind_non_zero = np.argwhere(x_mean[var_type][0, :, 0, 0] != 0)
        n = ind_non_zero.shape[0]
        if n > 0:
            if n < n_var:
                ind_non_zero = ind_non_zero[:, 0]
                x_out[var_type][:, ind_non_zero, :, :] -= x_mean[var_type][:, ind_non_zero, :, :]
            else:
                x_out[var_type] -= x_mean[var_type]
        ind_non_one = np.argwhere(x_std_inv[var_type][0, :, 0, 0] != 1)
        n = ind_non_one.shape[0]
        if n > 0:
            if n < n_var:
                ind_non_one = ind_non_one[:, 0]
                x_out[var_type][:, ind_non_one, :, :] *= x_std_inv[var_type][:, ind_non_one, :, :]
            else:
                x_out[var_type] *= x_std_inv[var_type]

    return x_out


def unstandardize(y, y_mean, y_std):
    """Unstandardizes the model predictions 'y' in the same fashion as the function standardize works on x."""

    y_out = dict()
    for var_type in y.keys():
        y_out[var_type] = y[var_type] * y_std[var_type] + y_mean[var_type]

    return y_out

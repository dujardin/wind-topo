#!/usr/bin/env python
# -*- coding: utf-8 -*-
# pep8: disable=E501,W504

"""This is a module providing the function "downscale" called by the scipt "main_downscaling.py".

It performs the downscaling task described in "main_downscaling.py", given the user inputs stored in the
dictionnary "user_in".
It returns "None", but generates 3 geotiff files and 2 animations (.gif and .mp4).
"""

__version__ = "0.1.0"
__date__ = "2022/03/14"
__author__ = "Jérôme Dujardin"
__credits__ = ["Jérôme Dujardin"]
__maintainer__ = "Jérôme Dujardin"
__email__ = "jerome.dujardin@slf.ch"
__license__ = "AGPL"

# Python Standard Library
import os
import time
import datetime as dt
import pickle
import json
import pathlib

# Installed packages
import numpy as np
import torch

# Project modules
import modules.windtopo as wt
import modules.windtopo_functions as wt_func
import modules.support_functions as func
from modules.animation import uv_geotiff2animation

from progsettings import NO_GPU, USE_GPU, MULTI_GPU, MINSPEED, MAXSPEED, MEAS_HEIGHT, MINELEVATION, MAXELEVATION


def downscale(user_in):

    t_start = time.time()

# %% Check user inputs

    for path, name in zip([user_in['path_dem'], user_in['path_trained_model_state'], user_in['path_standardization']],
                          ['DEM', 'Model state', 'Standardazition']):
        if not(path.is_file()):
            raise FileNotFoundError('%s file not found at %s' % (name, str(path)))

    for val in user_in['dem_crop_size'] + user_in['dem_ind_upleft']:
        if (type(val) is not(int)) or (val < 0):
            raise ValueError('Sizes and indices for cropping must be integer >= 0')

    if user_in['wind_vel_h0'] < 0:
        raise ValueError('Wind speed should be positive')

    if not(user_in['path_output'].parent.is_dir()):
        raise FileNotFoundError('The location of the output folder does not exist: %s' % str(user_in['path_output'].parent))
    if not(user_in['path_output'].is_dir()):
        os.mkdir(user_in['path_output'])
        func.log_print('Output directory created: %s' % str(user_in['path_output']), user_in['path_log'])

    if (user_in['wind_vel_h0'] > MAXSPEED) or (user_in['wind_vel_h0'] < MINSPEED):
        func.log_print(('WARNING: Wind-Topo was trained with wind speeds between %.2f and %.2f m/s at %.2f m.a.g.l.\n' +
                        'You set %.2f m/s at %.2f m.a.g.l.') %
                       (MINSPEED, MAXSPEED, MEAS_HEIGHT, user_in['wind_vel_h0'], user_in['h0']), user_in['path_log'])

    # Write the user_in dict in the log file after casting pathlib and datetime into str
    func.log_print('\n### New run at: %s ###\n' % str(dt.datetime.now()), user_in['path_log'])
    tmp = user_in.copy()
    for k in tmp.keys():
        if isinstance(tmp[k], pathlib.PurePath) or isinstance(tmp[k], dt.date):
            tmp[k] = str(tmp[k])
    tmp['wind_azi_h0_list'] = [float(i) for i in tmp['wind_azi_h0_list']]  # JSON crash if not cast (again) into float
    with open(user_in['path_log'], 'a+') as file:
        file.write(json.dumps(tmp, indent=1))
    del tmp

    # Some prints
    if NO_GPU:
        txt = 'No GPU found, using CPU ...'
    elif not(USE_GPU):
        txt = 'GPU found but CPU run requested ...'
    elif not(MULTI_GPU):
        txt = 'Using 1 GPU ...'
    else:
        txt = 'Using 2 GPUs ...'
    func.log_print(txt, user_in['path_log'])
    func.log_print('Batch size is %d. Foveal blur block size is %d.' % (user_in['batch_size'], user_in['blur_block_size']),
                   user_in['path_log'])


# %% Initialize Wind-Topo

    # Parameters needed to instantiate the WindTopo class (static parameters arising from how Wind-Topo was trained)
    # See 'tech_documentation.pdf' for more information
    model_name = 'FusionCnn'  # WindTopo class contains several archi. Parameters stored in "model_state" are for "FusionCnn"
    n_new_topos = 6  # This architecture uses 6 topo descriptors computed on the fly
    delta_n_layer = 4  # 2 of the initial topo descriptors are discarded. So, the archi. has to treat 4 supplementary inputs
    n_channel_var = {'1D': [5, 5, 5, 4, 1, 4] + n_new_topos * [1],  # u, v, w', dtheta_dz, qs, time, 6 new topos on the fly
                     '2Dlr': [5, 5, 5, 4, 1, 1],   # u, v, w', dtheta_dz, qs, z_nwp
                     '2Dhr': [1] + n_new_topos * [1]}  # z_nwp and 6 new topos on the fly
    new_topo_info = {'ind_slope_2Dhr': [1], 'ind_aspect_2Dhr': [2],  # 2Dhr data must be ordered: z, slope, aspect
                     'ind_slope_1D': [-2], 'ind_aspect_1D': [-1],  # 1D must be ordered: u,v,w',dtheta_dz,qs,time,slope,aspect
                     'delta_n_layer': delta_n_layer}
    model_static_info = {'ind_dir_u_1D': 3,  # 4th layer of u_1D (top to ground), u is first variable in 1D data
                         'ind_dir_v_1D': 5,  # Same from v, which comes right after u in 1D data
                         'new_topo_info': new_topo_info}

    # Instantiate the model and set its parameters to the optimized values
    wt_model = wt.WindTopo(n_channel_var, model_name, model_static_info)
    if torch.cuda.device_count() > 1:
        wt_model.load_state_dict(torch.load(user_in['path_trained_model_state']))
    else:
        wt_model.load_state_dict(torch.load(user_in['path_trained_model_state'], map_location=torch.device('cpu')))

    # Some info from the model needed to generate its inputs
    dem_res = wt_model.res_hr
    nwp_patch_size = wt_model.size_in_lr
    dem_patch_size = wt_model.size_ori_hr
    topo_patch_full_size = wt_model.size_in_hr_full
    topo_patch_zoom_size = wt_model.size_in_hr_zoom
    ind_ctr_2Dlr = np.round(0.5 * (np.array(nwp_patch_size) - 1)).astype(np.int32)
    ind_ctr_2Dhr = np.round(0.5 * (np.array(topo_patch_zoom_size) - 1)).astype(np.int32)  # Central value from zoom patch


# %% Read data

    # Read DEM, crop it and compute slope and aspect
    (dem, _, _, geotransform, projection) = func.read_geotiff(user_in['path_dem'], [0])
    dem = dem[0, :, :]  # Removes singleton dimension

    if (user_in['dem_crop_size'][0] != 0) & (user_in['dem_crop_size'][1] != 0):
        if (user_in['dem_ind_upleft'][0] + user_in['dem_crop_size'][0] + dem_patch_size[0] - 1) > dem.shape[0]:
            raise ValueError(('Vertically, the cropping size and start index extend beyond the DEM height (%d px), ' +
                              '(given the required %d px margins)') % (dem.shape[0], 0.5 * (dem_patch_size[0] - 1)))
        if (user_in['dem_ind_upleft'][1] + user_in['dem_crop_size'][1] + dem_patch_size[1] - 1) > dem.shape[1]:
            raise ValueError(('Horizontally, the cropping size and start index extend beyond the DEM width (%d px), ' +
                              '(given the required %d px margins)') % (dem.shape[1], 0.5 * (dem_patch_size[1] - 1)))
        dem = dem[user_in['dem_ind_upleft'][0]:user_in['dem_ind_upleft'][0] + user_in['dem_crop_size'][0] + dem_patch_size[0] - 1,
                  user_in['dem_ind_upleft'][1]:user_in['dem_ind_upleft'][1] + user_in['dem_crop_size'][1] + dem_patch_size[1] - 1]
    else:
        user_in['dem_ind_upleft'] = [0, 0]  # In case there was some value despite no cropping (would shift the location, see below)
    slope, aspect = wt_func.compute_slope_aspect(dem, dem_res)

    # Offset x and y coord of upper left corner accordoing to crop and to lost band
    geotransform = list(geotransform)
    geotransform[0] += geotransform[1] * (user_in['dem_ind_upleft'][1] + 0.5 * (dem_patch_size[1] - 1))
    geotransform[3] += geotransform[5] * (user_in['dem_ind_upleft'][0] + 0.5 * (dem_patch_size[0] - 1))

    # Save part of the dem corresponding to the area that will be dowsncaled (remove margins)
    a, b = round(0.5 * (dem_patch_size[0] - 1)), round(0.5 * (dem_patch_size[1] - 1))
    dem_cropped = dem[None, a:-a, b:-b]
    func.array2geotiff(dem_cropped, user_in['path_output'] / (user_in['file_out_rootname'] + '_dem.tif'), geotransform, projection)
    func.log_print('Cropped DEM saved in %s' % str(user_in['path_output']), user_in['path_log'])
    del dem_cropped, a, b

    # Check range of elevation and scale it if necessary
    dem, msg = func.scale_dem(dem, MINELEVATION, MAXELEVATION)
    if msg != '':
        func.log_print(msg, user_in['path_log'])

    # Load the values used during training to standardize the model inputs and un-standardize its outputs
    with open(user_in['path_standardization'], "rb") as fid:
        x_mean, x_std_inv, y_mean, y_std, _, _, _ = pickle.load(fid)


# %% Generate the synthetic low-resolution (nwp) input data (here: uniform fields along x/y plane)

    height_list = np.array([1164, 598, 293, 89, 10], dtype=np.float32)  # (m) Height levels used during Wind-Topo training
    vel = user_in['wind_vel_h0'] * np.log((height_list - user_in['d']) / user_in['z0']) / \
        np.log((user_in['h0'] - user_in['d']) / user_in['z0'])
    u = - vel * np.sin(user_in['wind_azi_h0_list'][0] * np.pi / 180)
    v = - vel * np.cos(user_in['wind_azi_h0_list'][0] * np.pi / 180)
    u = np.tile(u[None, :, None, None], [1, 1] + nwp_patch_size)
    v = np.tile(v[None, :, None, None], [1, 1] + nwp_patch_size)
    vel = np.tile(vel[None, :, None, None], [1, 1] + nwp_patch_size)

    w_prime = np.full_like(u, 0)
    dtheta_dz = np.full([1, 4] + nwp_patch_size, user_in['grad_theta'], dtype=np.float32)
    qs = np.zeros([1, 1] + nwp_patch_size, dtype=np.float32)
    z_nwp = np.full([1, 1] + nwp_patch_size, dem.mean(), dtype=np.float32)
    timefeatures = wt_func.generate_time_features([user_in['timestamp']])


# %% Model inputs

    x = {'1D': None, '2Dlr': None, '2Dhr_full': None, '2Dhr_zoom': None}
    x_2Dlr_temp = np.repeat(np.concatenate((u, v, w_prime, dtheta_dz, qs, z_nwp), 1), user_in['batch_size'], 0)
    x_1D_temp = np.concatenate((x_2Dlr_temp[:, :-1, [ind_ctr_2Dlr[0]], ind_ctr_2Dlr[1], None],
                                np.repeat(timefeatures[:, :, None, None], user_in['batch_size'], 0)), 1)  # -1 to remove z_nwp
    del u, v, w_prime, dtheta_dz, qs, z_nwp, timefeatures

    model_dyn_info = {'is_training': False, 'angle_rot': None}  # Used only during training
    is_split = False  # Spliting option for the prediction (see class WindTopo)
    ind_2Dhr_treat = None  # Used only when is_split is True
    outputs_conv_hr_pre = None  # Same


# %% Loops on grid points and wind directions

    # Precompute some information needed for patch extraction and foveal blur
    patch_info = func.info_for_extract_patches(dem.shape, dem_patch_size, topo_patch_full_size, topo_patch_zoom_size)
    blur_info_full = wt_func.fovel_blur_info(topo_patch_full_size, user_in['blur_use_pytorch'])
    blur_info_zoom = blur_info_full  # For now, same blur for full and zoom

    # Output arrays and number of batches
    n_dir = len(user_in['wind_azi_h0_list'])
    n_row_out = dem.shape[0] - (dem_patch_size[0] - 1)
    n_col_out = dem.shape[1] - (dem_patch_size[1] - 1)
    u_downscaled = np.full((n_dir, n_row_out, n_col_out), np.nan)
    v_downscaled = np.full((n_dir, n_row_out, n_col_out), np.nan)
    n_batch = int(np.ceil(n_row_out * n_col_out / user_in['batch_size']))
    func.log_print('Number of batches: %d' % n_batch, user_in['path_log'])

    # Loop on grid points (n points in a batch)
    t0 = time.time()
    for i_batch in range(n_batch):
        txt = 'Computing batch %d out of %d' % (i_batch + 1, n_batch)
        if i_batch > 1:
            txt = txt + ' ------------------ Remaining time: %.2f min.' % ((time.time() - t0) * (n_batch - i_batch) / 60)
        func.log_print(txt, user_in['path_log'])
        t0 = time.time()

        # Extract patches of topo descriptors around each grid point
        t_extract = time.time()
        (patches, ind_row_pred, ind_col_pred) = func.extract_patches(patch_info, dem, slope, aspect, user_in['batch_size'],
                                                                     i_batch, user_in['resize_use_pytorch'])
        t_extract = time.time() - t_extract

        # Blur those patches
        t_blur = time.time()
        patches = func.blur_patches(patches, blur_info_full, blur_info_zoom, user_in['blur_block_size'])
        t_blur = time.time() - t_blur

        # Organize them
        patches_z_full = patches[0][:, None, :, :]
        patches_slope_full = patches[1][:, None, :, :]
        patches_aspect_full = patches[2][:, None, :, :]
        patches_z_zoom = patches[3][:, None, :, :]
        patches_slope_zoom = patches[4][:, None, :, :]
        patches_aspect_zoom = patches[5][:, None, :, :]
        batch_size_real = patches_z_full.shape[0]
        del patches

        # Generate some of Wind-Topo inputs
        x['2Dhr_full'] = np.concatenate((patches_z_full, patches_slope_full, patches_aspect_full), 1)
        x['2Dhr_zoom'] = np.concatenate((patches_z_zoom, patches_slope_zoom, patches_aspect_zoom), 1)
        del patches_z_full, patches_slope_full, patches_aspect_full, patches_z_zoom
        if i_batch == (n_batch - 1):  # Last batch: fewer points
            x_1D_temp = x_1D_temp[:batch_size_real, :, :, :]
            x_2Dlr_temp = x_2Dlr_temp[:batch_size_real, :, :, :]

        # Standization of the inputs that are ready
        t_stdhr = time.time()
        x = wt_func.standardize(x, x_mean, x_std_inv, ['2Dhr_full', '2Dhr_zoom'], False)
        t_stdhr = time.time() - t_stdhr

        # Loop through the wind directions
        t_stdlr = 0
        t_pred = 0
        for i_dir, wind_dir in enumerate(user_in['wind_azi_h0_list']):

            # Generate the remaining inputs
            wind_dir *= np.pi / 180
            u, v = -vel * np.sin(wind_dir), -vel * np.cos(wind_dir)
            x_2Dlr_temp[:, :5, :, :], x_2Dlr_temp[:, 5:10, :, :] = u, v
            x_1D_temp[:, :10, :, :] = x_2Dlr_temp[:, :10, [ind_ctr_2Dlr[0]], ind_ctr_2Dlr[1], None]
            x['1D'] = np.concatenate((x_1D_temp,
                                      patches_slope_zoom[:, :, [ind_ctr_2Dhr[0]], ind_ctr_2Dhr[1], None],
                                      patches_aspect_zoom[:, :, [ind_ctr_2Dhr[0]], ind_ctr_2Dhr[1], None]), 1)
            x['2Dlr'] = x_2Dlr_temp.copy()  # Deep copy because of standardization below (would keep do it on same data)

            # Standardize them
            t1 = time.time()
            x = wt_func.standardize(x, x_mean, x_std_inv, ['1D', '2Dlr'], False)
            t_stdlr += time.time() - t1

            # Use Wind-Topo
            t2 = time.time()
            yhat = wt_model.predict(x, model_dyn_info, is_split, ind_2Dhr_treat, outputs_conv_hr_pre)
            t_pred += time.time() - t2
            yhat = wt_func.unstandardize(yhat, y_mean, y_std)
            yhat = np.squeeze(yhat['1D'], (2, 3))

            # Write the predictions in the output arrays
            u_downscaled[i_dir, ind_row_pred, ind_col_pred] = yhat[:, 0]
            v_downscaled[i_dir, ind_row_pred, ind_col_pred] = yhat[:, 1]

        # Print information
        func.log_print('Patches extraction took:        %.2f s' % t_extract, user_in['path_log'])
        func.log_print('Foveal blur took:               %.2f s' % t_blur, user_in['path_log'])
        func.log_print('Standardizing topo inputs took: %.2f s' % t_stdhr, user_in['path_log'])
        func.log_print('Standardizing nwp inputs took:  %.2f s' % t_stdlr, user_in['path_log'])
        t_single = t_pred * 1e3 / (n_dir * batch_size_real)
        func.log_print('Wind-Topo predictions took:     %.2f s (%.2f ms/pt)' % (t_pred, t_single), user_in['path_log'])
        t_rest = time.time() - t0 - (t_extract + t_blur + t_stdhr + t_stdlr + t_pred)
        func.log_print('Rest (mostly concat) took:      %.2f s' % t_rest, user_in['path_log'])


# %% Save geotiff

    func.log_print('Saving geotiff files ...', user_in['path_log'])
    vel_downscaled = np.sqrt(u_downscaled**2 + v_downscaled**2)
    func.array2geotiff(u_downscaled, user_in['path_output'] / (user_in['file_out_rootname'] + '_u.tif'), geotransform, projection)
    func.array2geotiff(v_downscaled, user_in['path_output'] / (user_in['file_out_rootname'] + '_v.tif'), geotransform, projection)
    func.array2geotiff(vel_downscaled, user_in['path_output'] / (user_in['file_out_rootname'] + '_vel.tif'), geotransform, projection)
    func.log_print('geotiff files saved in %s' % str(user_in['path_output']), user_in['path_log'])


# %% Generate animation

    func.log_print('Generating animation ...', user_in['path_log'])
    filepath_input_u_v_dem = [user_in['path_output'] / (user_in['file_out_rootname'] + '_u.tif'),
                              user_in['path_output'] / (user_in['file_out_rootname'] + '_v.tif'),
                              user_in['path_output'] / (user_in['file_out_rootname'] + '_dem.tif')]
    # In .gif
    filepath_out = user_in['path_output'] / (user_in['file_out_rootname'] + '.gif')
    uv_geotiff2animation(filepath_input_u_v_dem, filepath_out, 'gif', '')
    # In .mp4
    if user_in['path_ffmpeg'] != '':
        if user_in['path_ffmpeg'].is_file():
            filepath_out = user_in['path_output'] / (user_in['file_out_rootname'] + '.mp4')
            try:
                uv_geotiff2animation(filepath_input_u_v_dem, filepath_out, 'mp4', str(user_in['path_ffmpeg']))
            except:  # noqa
                func.log_print(('Could not generate .mp4. Most likely a problem with ffmpeg library found at:\n%s\n' +
                                'On macOS, try to change the rights of the provided ffmpeg file, or right-click it > Open > Open\n' +
                                'If the file is corrupted, please use the provided original zipped file.') %
                               (str(user_in['path_ffmpeg'])), user_in['path_log'])
        else:
            func.log_print('ffmpeg could not be find at %s' % (str(user_in['path_ffmpeg'])), user_in['path_log'])

    func.log_print('Animations saved in %s' % str(user_in['path_output']), user_in['path_log'])


# %% End
    func.log_print('\n### Finished at: %s (took %.2f min) ###\n' %
                   (str(dt.datetime.now()), (time.time() - t_start) / 60), user_in['path_log'])

    return None

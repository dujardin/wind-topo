#!/usr/bin/env python
# -*- coding: utf-8 -*-
# pep8: disable=E501,W504

"""This is a module providing functions needed by the module "downscale_uniform".

Contrary to the module "windtopo_functions", those functions were developped for this downscaling example only.
They are however usefull (as such or some parts of them) to develop a 'real' downscaling scheme based on Wind-Topo.
"""

__version__ = "0.1.0"
__date__ = "2022/03/14"
__author__ = "Jérôme Dujardin"
__credits__ = ["Jérôme Dujardin"]
__maintainer__ = "Jérôme Dujardin"
__email__ = "jerome.dujardin@slf.ch"
__license__ = "AGPL"

# Installed packages
import numpy as np
from osgeo import gdal, osr
import torch
import torchvision
try:
    # In not using Pytorch for it, function required for resizing the "full" patches
    from skimage.transform import resize
    skimage_available = True
except ImportError:
    skimage_available = False

# Project modules
from modules.windtopo_functions import foveal_blur
from progsettings import t_device, t_type, LOG_MSG, PRINT_MSG


torch.pi = torch.acos(torch.zeros(1)).item() * 2
t_resize_mode = torchvision.transforms.functional.InterpolationMode('bilinear')
t_resize = torchvision.transforms.functional.resize


def log_print(msg, path):
    """Prints the string 'msg' on the screen and / or writes it in the text file located in 'path'."""

    try:
        if PRINT_MSG:
            print(msg)
        if LOG_MSG:
            # now = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            # to_write = now + ' ' + msg
            to_write = msg
            with open(path, 'a+') as f_id:
                f_id.write(to_write + '\n')
    except FileNotFoundError:
        pass  # No raise: Write log is not critical


def info_for_extract_patches(dem_shape, dem_patch_size, topo_patch_full_size, topo_patch_zoom_size):
    """Generates a dict 'info' with all (static) information needed to extract the patchs from the input topo descriptors"""

    # Info to extract the patches for dem / slope / aspect around each point of interest
    dem_n, dem_m = dem_shape
    hr_n, hr_m = tuple(dem_patch_size)
    full_n, full_m = tuple(topo_patch_full_size)
    zoom_n, zoom_m = tuple(topo_patch_zoom_size)
    ul_col, ul_row = np.meshgrid(np.arange(0, dem_m - (hr_m - 1)), np.arange(0, dem_n - (hr_n - 1)))
    ur_col, _ = np.meshgrid(np.arange(hr_m, dem_m + 1), np.arange(0, dem_n - (hr_n - 1)))
    _, ll_row = np.meshgrid(np.arange(0, dem_m - (hr_m - 1)), np.arange(hr_n, dem_n + 1))
    ul_col, ul_row, ur_col, ll_row = ul_col.ravel(), ul_row.ravel(), ur_col.ravel(), ll_row.ravel()

    # Info to crop the central "zoom" domains
    row_start_z = round(0.5 * (hr_n - zoom_n))
    row_stop_z = row_start_z + zoom_n  # noqa (despite linter warning, used in eval below)
    col_start_z = round(0.5 * (hr_m - zoom_m))
    col_stop_z = col_start_z + zoom_m  # noqa

    info = dict()
    keys = ['hr_n', 'hr_m', 'full_n', 'full_m', 'zoom_n', 'zoom_m',
            'ul_col', 'ul_row', 'ur_col', 'll_row',
            'row_start_z', 'row_stop_z', 'col_start_z', 'col_stop_z']
    for k in keys:
        info[k] = eval(k)
    return info


def extract_patches(f, z, slope, aspect, batch_size, i_batch, use_pytorch):
    """Extracts patches from 2D arrays 'z', 'slope', and 'aspect' given the information provided by the dict 'f'."""

    ind_batch = [i_batch * batch_size + i for i in range(batch_size)
                 if i_batch * batch_size + i < f['ul_col'].shape[0]]
    batch_size_real = len(ind_batch)
    patches_z_hr = np.empty((batch_size_real, f['hr_n'], f['hr_m']), dtype=np.float32)
    patches_slope_hr = np.empty((batch_size_real, f['hr_n'], f['hr_m']), dtype=np.float32)
    patches_aspect_hr = np.empty((batch_size_real, f['hr_n'], f['hr_m']), dtype=np.float32)
    ind_row_pred = np.empty((batch_size_real,), dtype=np.int32)
    ind_col_pred = np.empty((batch_size_real,), dtype=np.int32)
    for i, ind in enumerate(ind_batch):
        patches_z_hr[i, :, :] = z[f['ul_row'][ind]:f['ll_row'][ind],
                                  f['ul_col'][ind]:f['ur_col'][ind]]
        patches_slope_hr[i, :, :] = slope[f['ul_row'][ind]:f['ll_row'][ind],
                                          f['ul_col'][ind]:f['ur_col'][ind]]
        patches_aspect_hr[i, :, :] = aspect[f['ul_row'][ind]:f['ll_row'][ind],
                                            f['ul_col'][ind]:f['ur_col'][ind]]
        ind_row_pred[i] = f['ul_row'][ind]
        ind_col_pred[i] = f['ul_col'][ind]

    # "full" data
    if ~use_pytorch & ~skimage_available & (i_batch == 0):
        print('For resizing full patches in support_functions.py: Could not find package scikit-image. ' +
              'Will use Pytorch instead (much faster anyway).\n')
    if use_pytorch or not(skimage_available):

        with torch.no_grad():
            new_size = [f['full_n'], f['full_m']]
            patches_z_hr_tensor = torch.tensor(patches_z_hr, dtype=t_type, device=t_device)
            patches_slope_hr_tensor = torch.tensor(patches_slope_hr, dtype=t_type, device=t_device)
            patches_aspect_hr_tensor = torch.tensor(patches_aspect_hr, dtype=t_type, device=t_device)
            patches_z_full = t_resize(patches_z_hr_tensor, new_size, t_resize_mode, antialias=True)
            patches_slope_full = t_resize(patches_slope_hr_tensor, new_size, t_resize_mode, antialias=True)
            cos_aspect = t_resize(torch.cos(patches_aspect_hr_tensor), new_size, t_resize_mode, antialias=True)
            sin_aspect = t_resize(torch.sin(patches_aspect_hr_tensor), new_size, t_resize_mode, antialias=True)
            patches_aspect_full = torch.fmod(torch.atan2(sin_aspect, cos_aspect) + 2 * torch.pi, 2 * torch.pi)
            del patches_z_hr_tensor, patches_slope_hr_tensor, patches_aspect_hr_tensor, cos_aspect, sin_aspect
            patches_z_full = patches_z_full.cpu().numpy()
            patches_slope_full = patches_slope_full.cpu().numpy()
            patches_aspect_full = patches_aspect_full.cpu().numpy()

    else:
        cos_aspect_hr, sin_aspect_hr = np.cos(patches_aspect_hr), np.sin(patches_aspect_hr)
        patches_z_full = np.empty((batch_size_real, f['full_n'], f['full_m']), dtype=np.float32)
        patches_slope_full = np.empty((batch_size_real, f['full_n'], f['full_m']), dtype=np.float32)
        patches_aspect_full = np.empty((batch_size_real, f['full_n'], f['full_m']), dtype=np.float32)
        new_size = (f['full_n'], f['full_m'])
        for i in range(batch_size_real):
            patches_z_full[i, :, :] = resize(patches_z_hr[i, :, :], new_size, anti_aliasing=True)
            patches_slope_full[i, :, :] = resize(patches_slope_hr[i, :, :], new_size, anti_aliasing=True)
            cos_aspect = resize(cos_aspect_hr[i, :, :], new_size, anti_aliasing=True)
            sin_aspect = resize(sin_aspect_hr[i, :, :], new_size, anti_aliasing=True)
            patches_aspect_full[i, :, :] = np.mod(np.arctan2(sin_aspect, cos_aspect), 2 * np.pi)
        del cos_aspect_hr, sin_aspect_hr

    # "zoom" data
    patches_z_zoom = patches_z_hr[:, f['row_start_z']:f['row_stop_z'], f['col_start_z']:f['col_stop_z']]
    patches_slope_zoom = patches_slope_hr[:, f['row_start_z']:f['row_stop_z'], f['col_start_z']:f['col_stop_z']]
    patches_aspect_zoom = patches_aspect_hr[:, f['row_start_z']:f['row_stop_z'], f['col_start_z']:f['col_stop_z']]

    patches = (patches_z_full, patches_slope_full, patches_aspect_full,
               patches_z_zoom, patches_slope_zoom, patches_aspect_zoom)

    return patches, ind_row_pred, ind_col_pred


def blur_patches(patches, info_full, info_zoom, block_size):
    """Calls the function foveal_blur from the module windtopo_functions for each full and zoom patch in the tupple 'patches'."""

    (patches_z_full, patches_slope_full, patches_aspect_full,
     patches_z_zoom, patches_slope_zoom, patches_aspect_zoom) = patches

    # Note: even if block_size = the shape of the 1st dim, the loop below is as fast as without it
    # (Same speed as, for example: patches_z_full = foveal_blur(patches_z_full, info))

    # Foveal blur on "full" data
    for i_block in range(int(np.ceil(patches_z_full.shape[0] / block_size))):
        a = block_size * i_block
        b = min(block_size * (i_block + 1), patches_z_full.shape[0])
        patches_z_full[a:b, :, :] = foveal_blur(patches_z_full[a:b, :, :], info_full)
        patches_slope_full[a:b, :, :] = foveal_blur(patches_slope_full[a:b, :, :], info_full)
        cos_aspect = np.cos(foveal_blur(patches_aspect_full[a:b, :, :], info_full))
        sin_aspect = np.sin(foveal_blur(patches_aspect_full[a:b, :, :], info_full))
        patches_aspect_full[a:b, :, :] = np.mod(np.arctan2(sin_aspect, cos_aspect), 2 * np.pi)
    del cos_aspect, sin_aspect

    # Foveal blur on "zoom" data
    for i_block in range(int(np.ceil(patches_z_zoom.shape[0] / block_size))):
        a = block_size * i_block
        b = min(block_size * (i_block + 1), patches_z_zoom.shape[0])
        patches_z_zoom[a:b, :, :] = foveal_blur(patches_z_zoom[a:b, :, :], info_zoom)
        patches_slope_zoom[a:b, :, :] = foveal_blur(patches_slope_zoom[a:b, :, :], info_zoom)
        cos_aspect = np.cos(foveal_blur(patches_aspect_zoom[a:b, :, :], info_zoom))
        sin_aspect = np.sin(foveal_blur(patches_aspect_zoom[a:b, :, :], info_zoom))
        patches_aspect_zoom[a:b, :, :] = np.mod(np.arctan2(sin_aspect, cos_aspect), 2 * np.pi)
    del cos_aspect, sin_aspect

    return (patches_z_full, patches_slope_full, patches_aspect_full,
            patches_z_zoom, patches_slope_zoom, patches_aspect_zoom)


def read_geotiff(filepath, ind_band_list):
    """Imports the bands (channels) specified in 'ind_band_list' from the geotiff located in 'filepath'.

    Returns 'data' a (n_band, n_row, n_col) array, the arrays of grid point coordinates 'y' and 'x',
    the 'geotransform' and 'projection'."""

    raster = gdal.Open(str(filepath.absolute()))
    n_band = raster.RasterCount
    if ind_band_list != []:
        assert(max(ind_band_list) < n_band)

    geotransform = raster.GetGeoTransform()
    projection = raster.GetProjectionRef()

    x0 = geotransform[0]  # Upper left corner coord
    y0 = geotransform[3]

    dx = geotransform[1]  # Size of a pixel
    dy = geotransform[5]  # <0, good for all below

    x0 = x0 + 0.5 * dx
    y0 = y0 + 0.5 * dy

    if ind_band_list == []:
        ind_band_list2 = [i for i in range(n_band)]
    else:
        ind_band_list2 = ind_band_list
    for i, ind_band in enumerate(ind_band_list2):
        band = raster.GetRasterBand(1 + ind_band)
        if i == 0:
            data = np.zeros((len(ind_band_list2), band.YSize, band.XSize), dtype=np.float32)
        data[i, :, :] = band.ReadAsArray(0, 0, band.XSize, band.YSize)
    x = x0 + np.arange(0, data.shape[2]) * dx
    x = np.repeat(x[np.newaxis, :], data.shape[0], axis=0)
    y = y0 + np.arange(0, data.shape[1]) * dy
    y = np.repeat(y[:, np.newaxis], data.shape[2], axis=1)

    return (data, y, x, geotransform, projection)


def array2geotiff(array, filepath, geotransform, projection):
    """Writes at 'filepath' a geotiff with 'geotransform' and 'projection' containing 'array' (n_band, n_row, n_col)."""

    driver = gdal.GetDriverByName('GTiff')
    raster = driver.Create(str(filepath), array.shape[2], array.shape[1],
                           array.shape[0], gdal.GDT_Float32)
    raster.SetGeoTransform(geotransform)
    rasterSRS = osr.SpatialReference()
    rasterSRS.ImportFromWkt(projection)
    raster.SetProjection(rasterSRS.ExportToWkt())
    for i, image in enumerate(array, 1):
        raster.GetRasterBand(i).WriteArray(image)
    raster = None
    return None


def scale_dem(dem, min_val, max_val):
    """Scales and / or offsets 'dem' to have values between 'min_val' and 'max_val'."""

    dem_min, dem_max = dem.min(), dem.max()
    dem_min0, dem_max0 = dem_min * 1, dem_max * 1  # *1 for deep copy
    modif = False
    if dem_min < min_val:
        offset = min_val - dem_min
        dem += offset
        dem_max += offset
        dem_min = min_val
        modif = True
    if dem_max > max_val:
        offset = min(dem_max - max_val, dem_min - min_val)
        dem -= offset
        dem_max -= offset
        dem_min -= offset
        modif = True
    if dem_max > max_val:
        dem = (dem - dem_min) / (dem_max - dem_min)  # 0 to 1
        dem = dem * (max_val - dem_min) + dem_min
        modif = True
    msg = ''
    if modif:
        msg = (('Elevations used for training were between %d and %d m.a.s.l.\n' +
                'The input DEM is between %.2f and %.2f. It is now scaled to %.2f -> %.2f m.a.s.l.') %
               (min_val, max_val, dem_min0, dem_max0, dem.min(), dem.max()))
    return dem, msg

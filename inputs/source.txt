This DEM is in lat/lon (WGS84) coordinate system and comes from the Shuttle Radar Topography Mission (SRTM).
Farr, T. G., and M. Kobrick, 2000, Shuttle Radar Topography Mission.
It was resampled to resolutions of (0.0006952380996548745626,-0.0004761904999999998244) deg
in order to obtain a quasi-orthonormal projection for Switzerland, with a 53-meter resolution (only +/- 1 meter change along x with latitude).
#!/usr/bin/env python
# -*- coding: utf-8 -*-
# pep8: disable=E501,W504

"""This is a script that downscales any 53-m DEM (or a part of it) from a uniform input wind field with several wind directions.

The inputs needed by Wind-Topo are automatically generated, given a desired vertical profile of wind speed and vertical gradient
of potential temperture. The downscaling is performed on the grid of the input DEM, minus 10.5 km margins required by the model.
This is done for a given list of wind directions and the various high-resolution fields generated (u, v and vel) are stacked and stored
in geotiff files that have the correct geolocation (with respect the input DEM). Animations in .gif and .mp4 are generated and
show the evolution of the wind field as the wind direction changes. Information about the model run can be printed and/or saved
in a log file. (user inputs would also be saved in the file).
It is important to keep in mind that Wind-Topo was trained using COSMO-1 as inputs, and not uniform (in x,y plane) fields of
atmospheric variables. This script aims at showing how to use Wind-Topo and it provides the basic tools needed to prepare the
required inputs. To get the best performance, Wind-Topo needs input data that is as close as possible to what COSMO-1 provided.
Consequently, data from (or derived from) a NWP model with a 1.1 km resolution, a terrain following coordinate system with height levels
at (on average) 1164, 598, 293, 89, 10 meters above ground is ideal.
Wind-Topo was trained with measurements from 261 automatic weather stations located in Switzerland with elevations ranging
from 203 to 3580 meters above mean sea level. Its behavior for other geographical regions, with different climates, topographies,
synoptic winds and elevations has not been tested.
"""

__version__ = "0.1.0"
__date__ = "2022/03/14"
__author__ = "Jérôme Dujardin"
__credits__ = ["Jérôme Dujardin"]
__maintainer__ = "Jérôme Dujardin"
__email__ = "jerome.dujardin@slf.ch"
__license__ = "AGPL"

# Python Standard Library
import os
import datetime as dt

# Installed packages
import numpy as np
import torch

# Project modules
from modules.downscale_uniform import downscale

from progsettings import USE_GPU, MULTI_GPU, ID_GPU_0, ID_GPU_1, PATH_ROOT, PATH_FFMPEG


# %% User paramaters ############################################################################################################

# Path to a 53 meter Digital Elevation Model (geotiff file)
#    (After removing the model margins (199 px all around), the provided dem lead to a 1080x1920 domain)
user_in = dict()
user_in['path_dem'] = PATH_ROOT / 'inputs' / 'dem.tif'
user_in['dem_crop_size'] = [180, 320]  # Height, Width in pixel of the desired region ([0, 0] to avoid cropping)
user_in['dem_ind_upleft'] = [675, 950]  # Row, Col in the input DEM of the upper left corner of the selected region

# Path to the model state that contains the values of the model' parameters after training
user_in['path_trained_model_state'] = PATH_ROOT / 'trained_model' / 'model_state_9'
user_in['path_standardization'] = PATH_ROOT / 'trained_model' / 'standardization.pkl'
user_in['path_ffmpeg'] = PATH_FFMPEG

# Output settings
user_in['path_output'] = PATH_ROOT / 'outputs'
user_in['file_out_rootname'] = 'example'
user_in['path_log'] = user_in['path_output'] / (user_in['file_out_rootname'] + '_log.txt')
if user_in['path_log'].is_file():
    os.remove(user_in['path_log'])  # To comment if want to append to an existing file

# Parameters to generate synthetic low-resolution NWP input data
user_in['h0'] = 10  # (m) Height for which the wind speed is given
user_in['wind_vel_h0'] = 5  # (m/s) Wind speed at that height
user_in['wind_azi_h0_list'] = list(np.arange(0, 360, 10))  # (deg) From north, clockwise
user_in['z0'] = 0.1  # (m) Roughness length
user_in['d'] = 0.5  # (m) Zero plane displacement
user_in['grad_theta'] = 0.0045  # (K/m) Vertical gradient of potential temperature (positive for stably stratified conditions)
user_in['timestamp'] = dt.datetime(2022, 2, 15, 13, 0)  # Year (does not matter), Month, Day, Hour, Minute (does not matter)

# Options about how some big calculations are performed
user_in['resize_use_pytorch'] = True  # x5 faster with Pytorch CPU (Xeon E5-2650 v4) compared to numpy, and x10 with GPU (RTX 2080 Ti)
user_in['blur_use_pytorch'] = True   # x3 faster with Pytorch CPU (Xeon E5-2650 v4) compared to numpy, and x100 with GPU (RTX 2080 Ti)
#  For info: Wind-Topo (predictions only) is x146 faster on GPU (RTX 2080 Ti) than on CPU (Xeon E5-2650 v4)

# Batch size based on GPU model (dev. on RTX2080Ti: batch_size=512, blur_block_size=512 --> 6.6 GB on GPU, 3.6GB on RAM)
if USE_GPU:
    mem = torch.cuda.get_device_properties(ID_GPU_0).total_memory
    if MULTI_GPU:
        mem = min(mem, torch.cuda.get_device_properties(ID_GPU_1).total_memory)
    if mem >= 8e9:  # GPU with 8 GB memory
        user_in['batch_size'] = 512
        user_in['blur_block_size'] = 512
    elif mem >= 4e9:
        user_in['batch_size'] = 256
        user_in['blur_block_size'] = 256
    else:
        user_in['batch_size'] = 128
        user_in['blur_block_size'] = 128
else:  # Run on CPU
    user_in['batch_size'] = 256  # 256 leads to 2.2 GB RAM use peak (without foveal blur)
    user_in['blur_block_size'] = 256   # 256 leads to 3.3 GB RAM use peak
if not user_in['blur_use_pytorch']:  # Fovel blur will use numpy
    user_in['blur_block_size'] = 256  # 256 leads to 4.4 GB RAM use peak

# ###############################################################################################################################

# %% Call downscaling function with various parameters

if __name__ == '__main__':

    user_in['file_out_rootname'] = 'example_vel5'
    user_in['wind_vel_h0'] = 5  # (m/s)
    downscale(user_in)
    user_in['file_out_rootname'] = 'example_vel10'
    user_in['wind_vel_h0'] = 10  # (m/s)
    downscale(user_in)

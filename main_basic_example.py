#!/usr/bin/env python
# -*- coding: utf-8 -*-
# pep8: disable=E501,W504

"""This is a script allowing to use the model Wind-Topo on random input data.

This script aims at showing the required inputs of the model and at checking the model performance
given the hardware configuation (GPU(s) model). The user can set the batch size "batch_size" and
number of repetiions "n_batch". The required times for prediction will be printed, allowing to find the best batch size.
The user can specify to run the model on CPU or 1 GPU or 2 GPUs in progsettings.py
"""

__version__ = "0.1.0"
__date__ = "2022/03/14"
__author__ = "Jérôme Dujardin"
__credits__ = ["Jérôme Dujardin"]
__maintainer__ = "Jérôme Dujardin"
__email__ = "jerome.dujardin@slf.ch"
__license__ = "AGPL"

# Python Standard Library
import time

# Installed packages
import torch
import numpy as np

# Project modules
import modules.windtopo as wt
from progsettings import PATH_ROOT, NO_GPU, USE_GPU, MULTI_GPU


# %% User Paramaters ############################################################################################################

# Parameters for running Wind-Topo
batch_size = 512
n_batch = 100

# Path to the model state that contains the values of the model' parameters after training
path_trained_model_state = PATH_ROOT / 'trained_model' / 'model_state_9'

# ###############################################################################################################################

# %% Prints

if NO_GPU:
    txt = 'No GPU found, using CPU ...'
elif not(USE_GPU):
    txt = 'GPU found but CPU run requested ...'
elif not(MULTI_GPU):
    txt = 'Using 1 GPU ...'
else:
    txt = 'Using 2 GPUs ...'
print(txt)


# %% Initialize Wind-Topo

# Parameters needed to instantiate the WindTopo class (static parameters arising from how Wind-Topo was trained)
# See 'tech_documentation.pdf' for more information
model_name = 'FusionCnn'  # WindTopo class contains several architectures. Parameters stored in "model_state" are for "FusionCnn"
n_new_topos = 6  # This architecture uses 6 topo descriptors computed on the fly
delta_n_layer = 4  # 2 of the initial topo descriptors are discarded. So, the architecture has to treat 4 supplementary inputs
n_channel_var = {'1D': [5, 5, 5, 4, 1, 4] + n_new_topos * [1],  # u, v, w', dtheta_dz, qs, time, 6 new topos on the fly
                 '2Dlr': [5, 5, 5, 4, 1, 1],   # u, v, w', dtheta_dz, qs, z_nwp
                 '2Dhr': [1] + n_new_topos * [1]}  # z and 6 new topos on the fly
new_topo_info = {'ind_slope_2Dhr': [1], 'ind_aspect_2Dhr': [2],  # 2Dhr data must be ordered: z, slope, aspect
                 'ind_slope_1D': [-2], 'ind_aspect_1D': [-1],  # 1D data must be ordered: u,v,w',dtheta_dz,qs,time,slope,aspect
                 'delta_n_layer': delta_n_layer}
model_static_info = {'ind_dir_u_1D': 3,  # 4th layer of u_1D (top to ground), u is first variable in 1D data
                     'ind_dir_v_1D': 8,  # Same from v, which comes right after u in 1D data
                     'new_topo_info': new_topo_info}

# Instantiate the model and set its parameters to the optimized values
wt_model = wt.WindTopo(n_channel_var, model_name, model_static_info)
if torch.cuda.device_count() > 1:
    wt_model.load_state_dict(torch.load(path_trained_model_state))
else:
    wt_model.load_state_dict(torch.load(path_trained_model_state, map_location=torch.device('cpu')))


# %% Model inputs (x would need to be standardized with the same values used for training Wind-Topo)

x = {'1D': None, '2Dlr': None, '2Dhr_full': None, '2Dhr_zoom': None}
x['1D'] = np.random.rand(batch_size, 26, 1, 1).astype(np.float32)
x['2Dlr'] = np.random.rand(batch_size, 21, 19, 19).astype(np.float32)
x['2Dhr_full'] = np.random.rand(batch_size, 3, 77, 77).astype(np.float32)
x['2Dhr_zoom'] = np.random.rand(batch_size, 3, 77, 77).astype(np.float32)
model_dyn_info = {'is_training': False, 'angle_rot': None}  # Used only during training
is_split = False  # Spliting option for the prediction (see class WindTopo)
ind_2Dhr_treat = None  # Used only when is_split is True
outputs_conv_hr_pre = None  # Same


# %% Run Wind-Topo

print('Computing ...')
t0 = time.time()
for i_batch in range(n_batch):
    print('Batch %d out of %d' % (i_batch + 1, n_batch))
    yhat = wt_model.predict(x, model_dyn_info, is_split, ind_2Dhr_treat, outputs_conv_hr_pre)
    # Wind-Topo currently generates a prediction (u and v) for the domain'center only (1D)
    yhat = np.squeeze(yhat['1D'], (2, 3))  # Removes the last 2 singleton dimensions: output is (batch_size, 2) np.float32
t1 = time.time()
print('The prediction for %d batchs of %d points took %.2f seconds (%.2f ms per point)' %
      (n_batch, batch_size, t1 - t0, (t1 - t0) / (n_batch * batch_size) * 1e3))

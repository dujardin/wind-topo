#!/usr/bin/env python
# -*- coding: utf-8 -*-
# pep8: disable=E501,W504

"""This is a module providing variables that are needed, but read-only, throughout the project.

The user can:
- Set if GPU(s) or CPU must be used for the deep learning part of Wind-Topo.
- Set which GPU(s) to use.
- Set if the information about the run should be printed and / or saved in a log file.
- Set the various path.
"""

__version__ = "0.1.0"
__date__ = "2022/03/14"
__author__ = "Jérôme Dujardin"
__credits__ = ["Jérôme Dujardin"]
__maintainer__ = "Jérôme Dujardin"
__email__ = "jerome.dujardin@slf.ch"
__license__ = "AGPL"

# Python Standard Library
import os
from pathlib import Path
from platform import system, architecture
from inspect import currentframe, getframeinfo

# Installed packages
import torch


# %% User parameters

USE_GPU = True  # If False (or if no GPU is found), CPU will be used
MULTI_GPU = False  # If True and 2 GPUs found, branch 0 (u) of Wind-Topo is done on GPU with ID: "ID_GPU_0" below. Similar for branch 1
ID_GPU_0 = 0  # <ID> (like in torch.device("cuda:<ID>") of the GPU used for branch 0
ID_GPU_1 = 1  # Same for branch 1. Taken in account only if MULTI_GPU is True
LOG_MSG = True  # True to saved information about the run in a log file (user inputs would also be saved in the file)
PRINT_MSG = True  # True to print this information


# %% Paths

# Files are found and saved in folders located next to the script "main_downscaling" (which is callable from any working directory)
PATH_ROOT = Path(os.path.dirname(os.path.abspath(__file__)))

# Path for ffmpeg
if system() == 'Windows':
    if architecture()[0] == '32bit':
        PATH_FFMPEG = PATH_ROOT / 'extra' / 'ffmpeg_4.2.1' / 'win32' / 'ffmpeg.exe'
    elif architecture()[0] == '64bit':
        PATH_FFMPEG = PATH_ROOT / 'extra' / 'ffmpeg_4.2.1' / 'win64' / 'ffmpeg.exe'
    else:
        PATH_FFMPEG = ''
        print('Problem while checking the windows architecture.')
elif system() == 'Darwin':  # MacOS
    PATH_FFMPEG = PATH_ROOT / 'extra' / 'ffmpeg_4.2.1' / 'mac64' / 'ffmpeg'
elif system() == 'Linux':
    if architecture()[0] == '32bit':
        PATH_FFMPEG = PATH_ROOT / 'extra' / 'ffmpeg_4.2.1' / 'linux32' / 'ffmpeg'
    elif architecture()[0] == '64bit':
        PATH_FFMPEG = PATH_ROOT / 'extra' / 'ffmpeg_4.2.1' / 'linux64' / 'ffmpeg'
    else:
        PATH_FFMPEG = ''
        print('Problem while checking the linux architecture.')
else:
    PATH_FFMPEG = ''
    print('ffmpeg not provided for your OS. Result animation will be .gif only')
    frameinfo = getframeinfo(currentframe())
    print('You can install it and provide the path in %s line %s' % (frameinfo.filename, frameinfo.lineno - 2))


# %% Parameters used during training (used for sanity checks only)

MINSPEED = 0.05  # (m/s)
MAXSPEED = 35  # (m/s)
MEAS_HEIGHT = 10  # (m.a.g.l)
MINELEVATION = 203  # (m.a.s.l)
MAXELEVATION = 3580  # (m.a.s.l)


# %% Set device and data type for Pytorch

NO_GPU = False
if torch.cuda.device_count() == 0:
    NO_GPU = True
    USE_GPU = False
elif torch.cuda.device_count() == 1 or not(MULTI_GPU):
    MULTI_GPU = False

if USE_GPU:
    torch.backends.cudnn.benchmark = True
    torch.cuda.init()
    torch.cuda.empty_cache()
    t_device = torch.device("cuda:%s" % str(ID_GPU_0))
    torch.cuda.set_device(t_device)
    t_type = torch.float32
else:
    t_device = torch.device("cpu")
    t_type = torch.float32
